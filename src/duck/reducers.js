import types from './types';

const INITIAL_STATE = {
  url: '',
  urlData: []
}
const reducer = (state=INITIAL_STATE, action) => {
  switch(action.type) {
    case types.fetchShowSummary: {
      const { urlData } = action;
      return {
        ...state,
        urlData
      }
    }

    default: return state;
  }
}

export default reducer;