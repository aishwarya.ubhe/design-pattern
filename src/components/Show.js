import React, {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

function Show() {
  useEffect(() => {
    fetchShows();
  },[])

  const [shows, setShows] = useState([]);
  const fetchShows = async () => {
    const data = await fetch('http://api.tvmaze.com/shows?page=1');
    const shows = await data.json();
    console.log(shows);
    setShows(shows);
    }
  return (
    <div>
      {shows.map(s => (
        <div key={s.id}>
        <Link to={`/show/${s.id}`}>{s.name}</Link>
        </div>
        ))}
    </div>
  )
}

export default Show