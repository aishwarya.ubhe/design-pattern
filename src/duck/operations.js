import fetch from 'cross-fetch';
import Creators from './actions';

const fetchShowSummaryAction = Creators.fetchShowSummary;

export default {
  fetchShowSummaryAction
}
