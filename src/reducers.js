import { combineReducers } from 'redux';
import  reducer  from './duck/reducers'

const rootReducer = combineReducers(reducer);

export default rootReducer;