import React,{Component} from 'react';
import Nav from './components/Nav'
import Home from './components/Home'
import Show from './components/Show'
import ShowSummary from './containers/ShowSummary'
// import Pagination from './app/home/Pagination'
import { BrowserRouter as Router, Switch,Route } from 'react-router-dom'
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <div className="App">
        <Nav />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/show" exact component={Show} />
          <Route path="/show/:id" component={ShowSummary} />
        </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
